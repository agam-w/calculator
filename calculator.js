function calculator(x,y,method) {
    if (typeof x !== 'number'|| typeof y !== 'number') {
      return console.log("Input harus angka");
    };
   
    switch (method) {
      case 'tambah':
        console.log(x + y);
        break;
      case 'kurang':
        console.log(x - y);
        break;
      case 'kali':
        console.log(x * y);
        break;
      case 'bagi':
        console.log(x / y);
        break;
    }
    
    /* Bikin conditional disini dimana kalau methodnya tambah, nanti jalananin tambah dan seterusnya */
  }
  
  calculator(1,2,'tambah');
  calculator(1,2,'kurang');
  calculator(4,2,'bagi');
  calculator(1,2,'kali');
  